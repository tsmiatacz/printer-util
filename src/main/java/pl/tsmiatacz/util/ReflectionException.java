package pl.tsmiatacz.util;

class ReflectionException extends RuntimeException {

    ReflectionException(final Throwable t) {
        super(t);
    }
}
