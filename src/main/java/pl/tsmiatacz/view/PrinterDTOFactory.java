package pl.tsmiatacz.view;

import pl.tsmiatacz.model.impl.DefaultPrintServiceRepository;
import pl.tsmiatacz.model.PrintServiceRepository;

import javax.print.PrintService;
import javax.print.attribute.standard.Media;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PrinterDTOFactory {

    private static final PrinterDTOFactory INSTANCE = new PrinterDTOFactory();

    private PrintServiceRepository printServiceRepository = DefaultPrintServiceRepository.instance();

    private PrinterDTOFactory() {
        // empty
    }

    /**
     * @param printService not null
     * @param defaultPrintService not null
     * @return not null
     */
    public PrinterDTO create(final PrintService printService, final Optional<PrintService> defaultPrintService) {
        assert (printService != null);
        assert (defaultPrintService != null);

        return new PrinterDTO(
                printService.getName(),
                defaultPrintService
                        .map(printService::equals)
                        .orElse(false),
                createTrays(printService)
        );
    }

    private List<PrinterTrayDTO> createTrays(final PrintService printService) {
        return printServiceRepository.getTrays(printService).stream()
                .map(Media::toString)
                .map(PrinterTrayDTO::new)
                .sorted()
                .collect(Collectors.toList());
    }

    public static PrinterDTOFactory instance() {
        return INSTANCE;
    }
}
