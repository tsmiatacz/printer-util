package pl.tsmiatacz.model.impl;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;

class PrintServiceLookupWrapper {

    private static final PrintServiceLookupWrapper INSTANCE = new PrintServiceLookupWrapper();

    private PrintServiceLookupWrapper() {
        // empty
    }

    PrintService[] lookupPrintServices() {
        return PrintServiceLookup.lookupPrintServices(null, null);
    }

    PrintService lookupDefaultPrintService() {
        return PrintServiceLookup.lookupDefaultPrintService();
    }

    static PrintServiceLookupWrapper instance() {
        return INSTANCE;
    }
}
