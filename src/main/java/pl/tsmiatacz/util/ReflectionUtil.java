package pl.tsmiatacz.util;

public class ReflectionUtil {

    private ReflectionUtil() {
        // empty
    }

    /**
     * Check if object is instance of class with given name.
     *
     * @param obj       not null
     * @param qualifier not null
     */
    public static boolean isInstance(final Object obj, final String qualifier) {
        assert (obj != null);
        assert (qualifier != null);

        try {
            Class clazz = Class.forName(qualifier);
            return clazz.isInstance(obj);
        } catch (ClassNotFoundException e) {
            throw new ReflectionException(e);
        }
    }
}
