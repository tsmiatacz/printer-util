package pl.tsmiatacz.view;

import java.util.Comparator;
import java.util.List;

public class PrinterDTO implements Comparable<PrinterDTO> {

    private String name;
    private boolean isDefault;
    private List<PrinterTrayDTO> trays;

    /**
     * @param name not null
     * @param isDefault
     * @param trays not null
     */
    PrinterDTO(final String name, final boolean isDefault, final List<PrinterTrayDTO> trays) {
        assert (name != null);
        assert (trays != null);

        this.name = name;
        this.isDefault = isDefault;
        this.trays = trays;
    }

    /**
     * @return not null
     */
    public String name() {
        return name;
    }

    public boolean isDefault() {
        return isDefault;
    }

    /**
     * @return not null
     */
    public List<PrinterTrayDTO> trays() {
        return trays;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        if (isDefault) {
            sb.append("[default] ");
        }

        sb.append(name());

        if (!trays.isEmpty()) {
            sb.append("\n");
            trays.forEach(sb::append);
        }

        return sb.append("\n").toString();
    }

    @Override
    public int compareTo(final PrinterDTO other) {
        return Comparator.comparing(PrinterDTO::isDefault).reversed()
                .thenComparing(Comparator.comparing(PrinterDTO::name))
                .compare(this, other);
    }
}
