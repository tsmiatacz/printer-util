package pl.tsmiatacz;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.tsmiatacz.controller.PrinterListGUIController;
import pl.tsmiatacz.controller.PrinterListTextController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

public class Launcher {

    private static final Logger LOGGER = LogManager.getLogger("Main");

    private static final String PRINTER_JOB_KEY = "java.awt.printerjob";
    private static final String PRINTER_JOB_VALUE = "sun.awt.windows.WPrinterJob";

    private static final String INVALID_OS_MESSAGE = "Invalid PrinterJob implementation found (most likely you're not using Windows)";

    private static final Map<String, Supplier<Runnable>> CONTROLLER_MAPPING = ImmutableMap.of(
            "gui", PrinterListGUIController::new,
            "text", PrinterListTextController::new
    );

    public static void main(final String[] args) {
        try {
            run(args);
        } catch (Exception e) {
            LOGGER.error("Exception occured, terminating app", e);
        }
    }

    private static void run(final String[] args) {
        osCheck();

        final List<String> argsList = Optional.of(args)
                .map(Arrays::asList)
                .filter(CollectionUtils::isNotEmpty)
                .orElseGet(() -> ImmutableList.of("gui"));

        argsList.stream()
                .map(CONTROLLER_MAPPING::get)
                .map(Supplier::get)
                .forEach(Runnable::run);
    }

    private static void osCheck() {
        Optional.ofNullable(System.getProperty(PRINTER_JOB_KEY))
                .filter(PRINTER_JOB_VALUE::equals)
                .orElseThrow(() -> new IllegalStateException(INVALID_OS_MESSAGE));
    }
}
