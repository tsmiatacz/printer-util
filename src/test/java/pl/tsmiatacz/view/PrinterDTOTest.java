package pl.tsmiatacz.view;

import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.Test;

public class PrinterDTOTest {

    @Test
    public void shouldCompareByNameIfPrintersAreNotDefaultAndNameIsDifferent() throws Exception {
        // given
        final PrinterDTO printerDTOb = new PrinterDTO("b", false, Lists.emptyList());
        final PrinterDTO printerDTOa = new PrinterDTO("a", false, Lists.emptyList());

        // when
        final int resultBtoA = printerDTOb.compareTo(printerDTOa);
        final int resultAtoB = printerDTOa.compareTo(printerDTOb);

        // then
        Assertions.assertThat(resultBtoA).isEqualTo(1);
        Assertions.assertThat(resultAtoB).isEqualTo(-1);
    }

    @Test
    public void shouldBeEqualIfPrintersAreNotDefaultAndNameIsTheSame() throws Exception {
        // given
        final PrinterDTO printerDTOb = new PrinterDTO("c", false, Lists.emptyList());
        final PrinterDTO printerDTOa = new PrinterDTO("c", false, Lists.emptyList());

        // when
        final int resultBtoA = printerDTOb.compareTo(printerDTOa);
        final int resultAtoB = printerDTOa.compareTo(printerDTOb);

        // then
        Assertions.assertThat(resultBtoA).isEqualTo(0);
        Assertions.assertThat(resultAtoB).isEqualTo(0);
    }

    @Test
    public void shouldCompareByDefaultPrinterFirst() throws Exception {
        // given
        final PrinterDTO printerDTOb = new PrinterDTO("b", true, Lists.emptyList());
        final PrinterDTO printerDTOa = new PrinterDTO("a", false, Lists.emptyList());

        // when
        final int resultBtoA = printerDTOb.compareTo(printerDTOa);
        final int resultAtoB = printerDTOa.compareTo(printerDTOb);

        // then
        Assertions.assertThat(resultBtoA).isEqualTo(-1);
        Assertions.assertThat(resultAtoB).isEqualTo(1);
    }
}