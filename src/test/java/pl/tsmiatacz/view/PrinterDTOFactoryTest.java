package pl.tsmiatacz.view;

import com.google.common.collect.ImmutableList;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.tsmiatacz.model.PrintServiceRepository;

import javax.print.PrintService;
import javax.print.attribute.standard.Media;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PrinterDTOFactoryTest {

    private static final String PRINTER_NAME = "printer name";

    @InjectMocks
    private PrinterDTOFactory testObj;

    @Mock
    private PrintServiceRepository printServiceRepository;

    @Mock
    private PrintService printService;

    @Before
    public void setUp() throws Exception {
        when(printService.getName()).thenReturn(PRINTER_NAME);
    }

    @Test
    public void shouldCreateDefaultPrinter() throws Exception {
        // given
        when(printServiceRepository.getTrays(printService)).thenReturn(Lists.emptyList());

        // when
        final PrinterDTO printerDTO = testObj.create(printService, Optional.of(printService));

        // then
        Assertions.assertThat(printerDTO.name()).isEqualTo(PRINTER_NAME);
        Assertions.assertThat(printerDTO.isDefault()).isTrue();
    }

    @Test
    public void shouldCreateNonDefaultPrinter() throws Exception {
        // given
        when(printServiceRepository.getTrays(printService)).thenReturn(Lists.emptyList());

        // when
        final PrinterDTO printerDTO = testObj.create(printService, Optional.of(mock(PrintService.class)));

        // then
        Assertions.assertThat(printerDTO.name()).isEqualTo(PRINTER_NAME);
        Assertions.assertThat(printerDTO.isDefault()).isFalse();
    }

    @Test
    public void shouldCreateNonDefaultPrinterWhenThereIsNoDefaultPrintService() throws Exception {
        // given
        when(printServiceRepository.getTrays(printService)).thenReturn(Lists.emptyList());

        // when
        final PrinterDTO printerDTO = testObj.create(printService, Optional.empty());

        // then
        Assertions.assertThat(printerDTO.name()).isEqualTo(PRINTER_NAME);
        Assertions.assertThat(printerDTO.isDefault()).isFalse();
    }

    @Test
    public void shouldCreatePrinterWithNoTrays() throws Exception {
        // given
        when(printServiceRepository.getTrays(printService)).thenReturn(Lists.emptyList());

        // when
        final PrinterDTO printerDTO = testObj.create(printService, Optional.empty());

        // then
        Assertions.assertThat(printerDTO.trays()).isEmpty();
    }
    @Test
    public void shouldCreatePrinterWithProperTrays() throws Exception {
        // given
        final Media originalTray1 = mock(Media.class);
        when(originalTray1.toString()).thenReturn("b");
        final Media originalTray2 = mock(Media.class);
        when(originalTray2.toString()).thenReturn("a");

        final PrinterTrayDTO expectedTray1 = new PrinterTrayDTO("b");
        final PrinterTrayDTO expectedTray2 = new PrinterTrayDTO("a");

        when(printServiceRepository.getTrays(printService)).thenReturn(ImmutableList.of(originalTray1, originalTray2));

        // when
        final PrinterDTO printerDTO = testObj.create(printService, Optional.empty());

        // then
        Assertions.assertThat(printerDTO.trays())
                .usingFieldByFieldElementComparator()
                .containsExactly(expectedTray2, expectedTray1);
    }

}