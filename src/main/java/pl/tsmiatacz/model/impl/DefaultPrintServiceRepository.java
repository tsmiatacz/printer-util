package pl.tsmiatacz.model.impl;

import pl.tsmiatacz.model.PrintServiceRepository;
import pl.tsmiatacz.util.ReflectionUtil;

import javax.print.PrintService;
import javax.print.attribute.standard.Media;
import java.util.*;
import java.util.stream.Collectors;

public class DefaultPrintServiceRepository implements PrintServiceRepository {

    private static final DefaultPrintServiceRepository INSTANCE = new DefaultPrintServiceRepository();

    private static final String WIN32_MEDIA_TRAY_CLASS_NAME = "sun.print.Win32MediaTray";

    private PrintServiceLookupWrapper printServiceLookup = PrintServiceLookupWrapper.instance();

    private DefaultPrintServiceRepository(){
        // empty
    }

    @Override
    public Optional<PrintService> findDefaultPrintService() {
        return Optional.ofNullable(printServiceLookup.lookupDefaultPrintService());
    }

    @Override
    public List<PrintService> findAllPrintServices() {
        return Optional.ofNullable(printServiceLookup.lookupPrintServices())
                .map(Arrays::asList).orElseGet(Collections::emptyList);
    }

    @Override
    public Optional<PrintService> findPrintServiceByName(final String name) {
        assert (name != null);

        return findAllPrintServices().stream()
                .filter(ps -> ps.getName().equals(name))
                .findFirst();
    }

    @Override
    public PrintService getPrintServiceByName(final String name) {
        return findPrintServiceByName(name)
                .orElseThrow(() -> new NoSuchElementException(name));
    }

    @Override
    public List<Media> getTrays(final PrintService printService) {
        assert(printService != null);

        return Optional.ofNullable(printService.getSupportedAttributeValues(Media.class, null, null))
                .map(Media[].class::cast)
                .map(Arrays::asList)
                .orElseGet(Collections::emptyList).stream()
                .filter(media -> ReflectionUtil.isInstance(media, WIN32_MEDIA_TRAY_CLASS_NAME))
                .collect(Collectors.toList());
    }

    public static DefaultPrintServiceRepository instance() {
        return INSTANCE;
    }
}
