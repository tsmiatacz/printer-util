package pl.tsmiatacz.controller;

import pl.tsmiatacz.view.impl.DefaultPrinterProviderService;
import pl.tsmiatacz.view.PrinterDTO;
import pl.tsmiatacz.view.PrinterProviderService;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class PrinterListGUIController implements Runnable{

    private PrinterProviderService printerProviderService = DefaultPrinterProviderService.instance();

    @Override
    public void run() {
        final List<PrinterDTO> allPrinters = printerProviderService.findAllPrinters();
        createWindow(allPrinters);
    }

    private void createWindow(final List<PrinterDTO> allPrinters) {
        final PrinterCombo printerCombo = new PrinterCombo(allPrinters);
        final JPanel printerPanel = new JPanel();
        printerPanel.add(printerCombo);

        final TrayList trayList = new TrayList(printerCombo.getSelectedPrinterTrays());
        trayList.setSize(640, 320);

        printerCombo.addActionListener(e -> trayList.updateModel(printerCombo.getSelectedPrinterTrays()));

        final JPanel mainPanel = new JPanel(new GridLayout(0, 1));

        mainPanel.add(printerPanel);
        mainPanel.add(trayList);

        final JFrame jFrame = new JFrame("Printers list");
        jFrame.setSize(640, 480);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.add(mainPanel);
        jFrame.setVisible(true);
    }


}
