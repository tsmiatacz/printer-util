package pl.tsmiatacz.controller;

import pl.tsmiatacz.view.PrinterDTO;
import pl.tsmiatacz.view.PrinterTrayDTO;

import javax.swing.*;
import java.awt.*;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

class PrinterCombo extends JComboBox<PrinterDTO> {

    {
        setRenderer(this::getListCellRendererComponent);
    }

    /**
     * @param printers not null
     */
    PrinterCombo(final List<PrinterDTO> printers) {
        super();
        assert (printers != null);

        addAll(printers);
    }

    /**
     * @return not null
     */
    List<PrinterTrayDTO> getSelectedPrinterTrays() {
        return Optional.of(getSelectedIndex())
                .filter(i -> i > -1)
                .map(this::getItemAt)
                .map(PrinterDTO::trays)
                .orElseGet(Collections::emptyList);
    }

    private void addAll(final List<PrinterDTO> printers) {
        printers.forEach(this::addItem);
    }

    // renderer:

    private Component getListCellRendererComponent(final JList<? extends PrinterDTO> list, final PrinterDTO value,
                                                   final int index, final boolean isSelected, final boolean cellHasFocus) {
        final JLabel component = new JLabel();

        component.setOpaque(true);
        component.setText(labelText(index, value.name()));
        component.setBackground(value.isDefault() ? Color.PINK : Color.WHITE);

        return component;
    }

    private String labelText(final int index, final String name) {
        final StringBuilder sb = new StringBuilder();
        if (index > -1) {
            sb.append(index).append(". ");
        }
        sb.append(name);
        return sb.toString();
    }
}
