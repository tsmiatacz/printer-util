package pl.tsmiatacz.view.impl;

import pl.tsmiatacz.model.PrintServiceRepository;
import pl.tsmiatacz.model.impl.DefaultPrintServiceRepository;
import pl.tsmiatacz.view.PrinterDTO;
import pl.tsmiatacz.view.PrinterDTOFactory;

import javax.print.PrintService;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DefaultPrinterProviderService implements pl.tsmiatacz.view.PrinterProviderService {

    private static final DefaultPrinterProviderService INSTANCE = new DefaultPrinterProviderService();

    private PrintServiceRepository printServiceRepository = DefaultPrintServiceRepository.instance();
    private PrinterDTOFactory printerDTOFactory = PrinterDTOFactory.instance();

    private DefaultPrinterProviderService() {
        // empty
    }

    public List<PrinterDTO> findAllPrinters() {
        final Optional<PrintService> defaultPrintService = printServiceRepository.findDefaultPrintService();

        return printServiceRepository.findAllPrintServices().stream()
                .map(ps -> printerDTOFactory.create(ps, defaultPrintService))
                .sorted()
                .collect(Collectors.toList());
    }

    public static DefaultPrinterProviderService instance() {
        return INSTANCE;
    }
}
