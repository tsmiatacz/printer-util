package pl.tsmiatacz.model;

import javax.print.PrintService;
import javax.print.attribute.standard.Media;
import java.util.List;
import java.util.Optional;

public interface PrintServiceRepository {

    /**
     * @return not null
     */
    Optional<PrintService> findDefaultPrintService();

    /**
     * @return not null
     */
    List<PrintService> findAllPrintServices();

    /**
     * @param name not null
     * @return not null
     */
    Optional<PrintService> findPrintServiceByName(String name);

    /**
     * @param name not null
     * @return not null
     * @throws NoSuchElementException when serviceIsNotFound
     */
    PrintService getPrintServiceByName(String name);

    /**
     * @param printService not null
     * @return not null
     */
    List<Media> getTrays(PrintService printService);
}
