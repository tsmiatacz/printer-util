package pl.tsmiatacz.controller;

import pl.tsmiatacz.view.PrinterTrayDTO;

import javax.swing.*;
import java.util.List;

class TrayListModel extends DefaultListModel<PrinterTrayDTO> {

    private TrayListModel() {
        super();
    }

    static TrayListModel create(final List<PrinterTrayDTO> trays) {
        final TrayListModel trayListModel = new TrayListModel();
        trays.forEach(trayListModel::addElement);
        return trayListModel;
    }
}
