package pl.tsmiatacz.util;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class ReflectionUtilTest {

    @Test
    public void newAshouldBeInstanceOfA() throws Exception {
        // given
        final A a = new A();

        // when
        final boolean result = ReflectionUtil.isInstance(a, "pl.tsmiatacz.util.ReflectionUtilTest$A");

        // then
        Assertions.assertThat(result).isTrue();
    }

    @Test
    public void newBshouldBeInstanceOfA() throws Exception {
        // given
        final B b = new B();

        // when
        final boolean result = ReflectionUtil.isInstance(b, "pl.tsmiatacz.util.ReflectionUtilTest$A");

        // then
        Assertions.assertThat(result).isTrue();
    }

    @Test
    public void newCshouldNotBeInstanceOfA() throws Exception {
        // given
        final C c = new C();

        // when
        final boolean result = ReflectionUtil.isInstance(c, "pl.tsmiatacz.util.ReflectionUtilTest$A");

        // then
        Assertions.assertThat(result).isFalse();
    }

    @Test
    public void newAshouldNotBeInstanceOfB() throws Exception {
        // given
        final A a = new A();

        // when
        final boolean result = ReflectionUtil.isInstance(a, "pl.tsmiatacz.util.ReflectionUtilTest$B");

        // then
        Assertions.assertThat(result).isFalse();
    }

    @Test
    public void shouldThrowExceptionWhenThereIsNoClassWithGivenName() throws Exception {
        // given
        final A a = new A();

        // when
        Assertions.assertThatThrownBy(() -> ReflectionUtil.isInstance(a, "pl.tsmiatacz.util.ReflectionUtilTest$D"))

                //then
                .isInstanceOf(ReflectionException.class)
                .hasCauseInstanceOf(ClassNotFoundException.class);
    }

    // internal classes used for testing

    private class A {
        // empty
    }

    private class B extends A {
        // empty
    }

    private class C {
        // empty
    }
}