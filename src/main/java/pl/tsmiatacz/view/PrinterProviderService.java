package pl.tsmiatacz.view;

import java.util.List;

public interface PrinterProviderService {

    /**
     * @return not null
     */
    List<PrinterDTO> findAllPrinters();
}
