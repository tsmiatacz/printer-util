package pl.tsmiatacz.view;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class PrinterTrayDTOTest {

    @Test
    public void shouldCompareByNameIfTraysHaveDifferentName() throws Exception {
        // given
        final PrinterTrayDTO printerTrayDTOb = new PrinterTrayDTO("b");
        final PrinterTrayDTO printerTrayDTOa = new PrinterTrayDTO("a");

        // when
        final int resultBtoA = printerTrayDTOb.compareTo(printerTrayDTOa);
        final int resultAtoB = printerTrayDTOa.compareTo(printerTrayDTOb);

        // then
        Assertions.assertThat(resultBtoA).isEqualTo(1);
        Assertions.assertThat(resultAtoB).isEqualTo(-1);
    }

    @Test
    public void shouldBeEqualIfTraysHaveTheSameName() throws Exception {
        // given
        final PrinterTrayDTO printerTrayDTOb = new PrinterTrayDTO("c");
        final PrinterTrayDTO printerTrayDTOa = new PrinterTrayDTO("c");

        // when
        final int resultBtoA = printerTrayDTOb.compareTo(printerTrayDTOa);
        final int resultAtoB = printerTrayDTOa.compareTo(printerTrayDTOb);

        // then
        Assertions.assertThat(resultBtoA).isEqualTo(0);
        Assertions.assertThat(resultAtoB).isEqualTo(0);
    }
}