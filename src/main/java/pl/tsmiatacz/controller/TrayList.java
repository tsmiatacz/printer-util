package pl.tsmiatacz.controller;

import pl.tsmiatacz.view.PrinterTrayDTO;

import javax.swing.*;
import java.awt.*;
import java.util.List;

class TrayList extends JList<PrinterTrayDTO> {

    {
        setCellRenderer(this::getListCellRendererComponent);
    }

    TrayList(final List<PrinterTrayDTO> trays) {
        super();
        updateModel(trays);
    }

    void updateModel(final List<PrinterTrayDTO> trays) {
        setModel(TrayListModel.create(trays));
    }

    // renderer:

    private Component getListCellRendererComponent(final JList<? extends PrinterTrayDTO> list, final PrinterTrayDTO value,
                                                   final int index, final boolean isSelected, final boolean cellHasFocus) {
        final JLabel component = new JLabel();
        component.setText(labelText(index, value.name()));
        return component;
    }

    private String labelText(final int index, final String name) {
        final StringBuilder sb = new StringBuilder();
        if (index > -1) {
            sb.append(index).append(". ");
        }
        sb.append(name);
        return sb.toString();
    }
}
