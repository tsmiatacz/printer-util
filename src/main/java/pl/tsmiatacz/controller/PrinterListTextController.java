package pl.tsmiatacz.controller;

import pl.tsmiatacz.view.impl.DefaultPrinterProviderService;
import pl.tsmiatacz.view.PrinterProviderService;

public class PrinterListTextController implements Runnable {

    private PrinterProviderService printerProviderService = DefaultPrinterProviderService.instance();

    @Override
    public void run() {
        printerProviderService.findAllPrinters()
                .forEach(System.out::println);
    }
}
