package pl.tsmiatacz.model.impl;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.print.PrintService;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultPrintServiceRepositoryTest {

    @InjectMocks
    private DefaultPrintServiceRepository testObj;

    @Mock
    private PrintServiceLookupWrapper printServiceLookup;

    @Mock
    private PrintService printService1;

    @Mock
    private PrintService printService2;

    @Before
    public void setUp() throws Exception {
        when(printService1.getName()).thenReturn("a");
        when(printService2.getName()).thenReturn("b");
    }

    @Test
    public void shouldReturnEmptyWhenDefaultPrinterIsNotFound() throws Exception {
        // given
        when(printServiceLookup.lookupDefaultPrintService()).thenReturn(null);

        // when
        final Optional<PrintService> result = testObj.findDefaultPrintService();

        // then
        Assertions.assertThat(result).isEmpty();
    }

    @Test
    public void shouldReturnProperDefaultPrinter() throws Exception {
        // given
        when(printServiceLookup.lookupDefaultPrintService()).thenReturn(printService1);

        // when
        final Optional<PrintService> result = testObj.findDefaultPrintService();

        // then
        Assertions.assertThat(result).contains(printService1);
    }

    @Test
    public void shouldReturnEmptyWhenThereAreNoPrinters() throws Exception {
        // given
        when(printServiceLookup.lookupPrintServices()).thenReturn(new PrintService[] {});

        // when
        final List<PrintService> result = testObj.findAllPrintServices();

        // then
        Assertions.assertThat(result).isEmpty();
    }

    @Test
    public void shouldReturnEmptyWhenPrintersAreNull() throws Exception {
        // given
        when(printServiceLookup.lookupPrintServices()).thenReturn(null);

        // when
        final List<PrintService> result = testObj.findAllPrintServices();

        // then
        Assertions.assertThat(result).isEmpty();
    }

    @Test
    public void shouldReturnAllPrinters() throws Exception {
        // given
        when(printServiceLookup.lookupPrintServices()).thenReturn(new PrintService[] {printService1, printService2});

        // when
        final List<PrintService> result = testObj.findAllPrintServices();

        // then
        Assertions.assertThat(result).containsOnly(printService1, printService2);
    }

    @Test
    public void shouldFindPrinterByValidName() throws Exception {
        // given
        when(printServiceLookup.lookupPrintServices()).thenReturn(new PrintService[] {printService1, printService2});

        // when
        final Optional<PrintService> result = testObj.findPrintServiceByName("b");

        // then
        Assertions.assertThat(result).contains(printService2);
    }

    @Test
    public void shouldNotFindPrinterByInvalidName() throws Exception {
        // given
        when(printServiceLookup.lookupPrintServices()).thenReturn(new PrintService[] {printService1, printService2});

        // when
        final Optional<PrintService> result = testObj.findPrintServiceByName("c");

        // then
        Assertions.assertThat(result).isEmpty();
    }

    @Test
    public void shouldGetPrinterByValidName() throws Exception {
        // given
        when(printServiceLookup.lookupPrintServices()).thenReturn(new PrintService[] {printService1, printService2});

        // when
        final PrintService result = testObj.getPrintServiceByName("b");

        // then
        Assertions.assertThat(result).isEqualTo(printService2);
    }

    @Test
    public void shouldThrowExceptionOnAttemptToGetPrinterByInvalidName() throws Exception {
        // given
        when(printServiceLookup.lookupPrintServices()).thenReturn(new PrintService[] {printService1, printService2});

        // when
        Assertions.assertThatThrownBy(() -> testObj.getPrintServiceByName("c"))

                //then
                .isInstanceOf(NoSuchElementException.class)
                .hasMessage("c");
    }
}