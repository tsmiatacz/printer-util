package pl.tsmiatacz.view;

import java.util.Comparator;

public class PrinterTrayDTO implements Comparable<PrinterTrayDTO> {

    /**
     * @param name not null
     */
    PrinterTrayDTO(final String name) {
        assert (name != null);

        this.name = name;
    }

    private String name;

    /**
     * @return not null
     */
    public String name() {
        return name;
    }

    @Override
    public String toString() {
        return String.format("\t%s\n", name());
    }

    @Override
    public int compareTo(final PrinterTrayDTO other) {
        return Comparator.comparing(PrinterTrayDTO::name)
                .compare(this, other);
    }
}
