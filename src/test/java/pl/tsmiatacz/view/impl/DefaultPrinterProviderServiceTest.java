package pl.tsmiatacz.view.impl;

import com.google.common.collect.ImmutableList;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.tsmiatacz.model.PrintServiceRepository;
import pl.tsmiatacz.view.PrinterDTO;
import pl.tsmiatacz.view.PrinterDTOFactory;

import javax.print.PrintService;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultPrinterProviderServiceTest {

    @InjectMocks
    private DefaultPrinterProviderService testObj;

    @Mock
    private PrintServiceRepository printServiceRepository;

    @Mock
    private PrinterDTOFactory printerDTOFactory;

    @Mock
    private PrintService printService1;

    @Mock
    private PrintService printService2;

    @Mock
    private PrintService printService3;

    @Mock
    private PrinterDTO printerDTO1;

    @Mock
    private PrinterDTO printerDTO2;

    @Mock
    private PrinterDTO printerDTO3;

    @Before
    public void setUp() throws Exception {
        when(printerDTO1.name()).thenReturn("c");
        when(printerDTO2.name()).thenReturn("b");
        when(printerDTO3.name()).thenReturn("a");

        when(printerDTO1.isDefault()).thenReturn(false);
        when(printerDTO2.isDefault()).thenReturn(true);
        when(printerDTO3.isDefault()).thenReturn(false);

        when(printerDTO1.compareTo(any(PrinterDTO.class))).thenCallRealMethod();
        when(printerDTO2.compareTo(any(PrinterDTO.class))).thenCallRealMethod();
        when(printerDTO3.compareTo(any(PrinterDTO.class))).thenCallRealMethod();

        final Optional<PrintService> defaultPrintService = Optional.of(this.printService2);
        when(printServiceRepository.findDefaultPrintService()).thenReturn(defaultPrintService);

        when(printerDTOFactory.create(printService1, defaultPrintService)).thenReturn(printerDTO1);
        when(printerDTOFactory.create(printService2, defaultPrintService)).thenReturn(printerDTO2);
        when(printerDTOFactory.create(printService3, defaultPrintService)).thenReturn(printerDTO3);
    }

    @Test
    public void shouldReturnPrintersInProperOrder() throws Exception {
        // given
        when(printServiceRepository.findAllPrintServices()).thenReturn(ImmutableList.of(printService1, printService2, printService3));

        // when
        final List<PrinterDTO> result = testObj.findAllPrinters();

        // then
        Assertions.assertThat(result).containsExactly(printerDTO2, printerDTO3, printerDTO1);
    }

    @Test
    public void shouldReturnEmptyWhenThereAreNoPrinters() throws Exception {

    }
}